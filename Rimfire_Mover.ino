//Constants used in the code
  //Timeout for motor over travel time shutdown, this value was experimentally tuned to ~ 7 seconds
    const int TIMEOUT = 350;
  //Pausetime is the time for the motor to pause after taveling one direction, before starting in the other direction
    const int PAUSETIME = 4000;

//Setting variable states to zero
  int RightSensorState = 0; 
  int LeftSensorState = 0;
  int State = 0;
  int TimeoutCounter = 0;

void setup() {
  //Starting Serial Output over USB Serial connection for PC debugging
  Serial.begin(9600);
  // Setting pins 50-53 as outputs to control relay modules 1-4 respectivly
  pinMode(50,OUTPUT);
  pinMode(51,OUTPUT);
  pinMode(52,OUTPUT);
  pinMode(53,OUTPUT);
  // Setting pins 40&41 as inputs from L&R prox sensors respectivly
  // **NOTE** These inputs are currently configured as active LOW using the Arduino's internal pullup resister, this may need to be reconfigured when the actualy prox sensors are attached
  pinMode(40,INPUT_PULLUP);
  pinMode(41,INPUT_PULLUP);
  // Resetting all relay outputs to low on startup
  digitalWrite(50,HIGH);
  digitalWrite(51,HIGH);
  digitalWrite(52,HIGH);
  digitalWrite(53,HIGH);
  delay(500);
}

void loop() {
  // put your main code here, to run repeatedly:
  TimeoutCounter ++;
  RightSensorState = digitalRead(41);
  LeftSensorState = digitalRead(40);

  // Print all of the decision making data for this loop
  Serial.println("T");
  Serial.println(TimeoutCounter);
  Serial.println("R");
  Serial.println(RightSensorState);
  Serial.println("L");
  Serial.println(LeftSensorState);
  
  if (State == 0) {
    State = 1;
    State1();
    TimeoutCounter = 0;
    Serial.println("S");
    Serial.println(0001);
  }
  else if (State == 1 && (LeftSensorState == LOW || TimeoutCounter >= TIMEOUT)){
    State = 2;
    State2();
    TimeoutCounter = 0;
    Serial.println("S");
    Serial.println(1002);
  }
  else if (State == 2 && (RightSensorState == LOW || TimeoutCounter >= TIMEOUT)) {
    State = 1;
    State1();
    TimeoutCounter = 0;
    Serial.println("S");
    Serial.println(2001);
  }
}

void State1() {
   //State 1 is Right to Left
   //Stop all outputs
   digitalWrite(50,HIGH);
   digitalWrite(51,HIGH);
   digitalWrite(52,HIGH);
   digitalWrite(53,HIGH);
   //Timer for motor to stop
   delay(PAUSETIME);
   //Starting motor movement right to left
   digitalWrite(50,LOW);
   digitalWrite(51,LOW);
}

void State2() {
   //State 2 is Left to Right
   //Stop all outputs
   digitalWrite(50,HIGH);
   digitalWrite(51,HIGH);
   digitalWrite(52,HIGH);
   digitalWrite(53,HIGH);
   //Timer for motor to stop
   delay(PAUSETIME);
   //Starting motor movement Left to Right
   digitalWrite(52,LOW);
   digitalWrite(53,LOW);
}
